FROM postgres:14.3

RUN localedef -i et_EE -c -f UTF-8 -A /usr/share/locale/locale.alias et_EE.UTF-8

ADD init-scripts /docker-entrypoint-initdb.d
ADD manual-scripts /opt/scripts
RUN chmod +x -R /opt/scripts

ENV POSTGRES_PASSWORD=postgres
