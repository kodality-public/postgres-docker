# postgres-docker

# Run locally
```
docker run -d -p 5432:5432 --name db-name --restart unless-stopped -e ENV=dev \
  -v /Users/yuppi/db-name:/var/lib/postgresql/data \
  docker.kodality.com/postgres-docker:14.1
```
  
where `/Users/yuppi/db-name` is the local catalog where db data will be stored

# Create database
docker exec -e "DB_NAME=superdb" -e "USER_PREFIX=super" db-name /opt/scripts/createdb.sh

Optional env variables:
1. ADMIN_PASSWORD, defaults to 'test'
2. USER_PASSWORD, defaults to 'test'
3. POSTGRES_DB, defaults to 'postgres'
4. POSTGRES_USER, defaults to 'postgres'

# Drop database
 docker exec -e "DB_NAME=superdb" -e "USER_PREFIX=super" db-name /opt/scripts/dropdb.sh

